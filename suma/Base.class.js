function Base(){
  if (Object.keys(arguments).length < 2){
    throw new Error('Pending arguments');
  };
  this.values = arguments;
  this.runner = function(){};
  return this;
}

Base.prototype.run = function(){
  Object.keys(this.values).forEach(this.runner);
  return this;
};

module.exports = Base;