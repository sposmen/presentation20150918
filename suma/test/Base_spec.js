var assert = require('assert');
var should = require('should');
var sinon = require('sinon');

var Base = require("../Base.class");

describe("Base", function () {
  describe("defaults", function () {

    it('generate an error when is without arguments', function () {
      (function () { new Base() }).bind(null).should.throw();
    });

    it('generate an error when passing less than 1 arguments', function () {
      (function () { new Base(1) }).bind(null).should.throw();
    });

    it('set values as arguments', function () {
      var base = new Base(1, 2);
      base.values.should.be.arguments;
    });

    it("receives multiple arguments", function () {
      var base = new Base(1, 2);
      var runner = sinon.spy(base, 'runner');
      base.run();
      assert(runner.calledTwice);
    });
  })
});