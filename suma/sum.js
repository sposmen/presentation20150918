module.exports = function sum() {
  var values = arguments, result;

  Object.keys( arguments).forEach(function (index) {
    result = result ? result + values[index] : values[index];
  });

  return result;
};