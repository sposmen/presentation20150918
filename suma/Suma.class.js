var inherit = require('util').inherits,
  Base = require('./Base.class');

var bind = function(func, scope){
  return function(){ return func.apply(scope, arguments);
  };
};

function Sum(){
  Base.apply(this, arguments);
  this.runner = bind(this._sumIterator, this);
  return this;
}

// Here inherit generate a Base prototype reference to Sum
inherit(Sum, Base);

Sum.prototype._sumIterator = function (index) {
  this.result = this.result ? this.result + this.values[index] : this.values[index];
  return this;
};

module.exports = Sum;