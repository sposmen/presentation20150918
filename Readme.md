# Paradigma NodeJS

## Contents

1. `suma`: suma module with mocha TDD
2. `math`: module invoking suma
3. `general_web`: Express with http-proxy-middleware consuming template_endpoint microservice
4. `template_endpoint`: Express microservice with RethinkDB CRUD operations
5. `presentation`: RevealJS presentation of training 
6. `learnyounode`: learnyounode solutions 

## How load examples

Each content need an execution of `npm install` to install dependencies.
For presentation, you need to initialize the module `git submodule init && git submodule update`

1. For `suma` and `math` run `node index.js`.
2. For mocha in suma module (TDD) call `node_modules/.bin/mocha`
3. For `general_web` run `npm start` it runs internally as `node bin/www`
4. For `template_endpoint` run `PORT=3001 npm start` it runs internally as `node bin/www`. PORT is the channel that will express to listen
5. For `presentation` run `node_modules/.bin/grunt serve` it runs internally at *http://localhost:8000*
6. For `learnyounode` remember install it before run it (`npm install learnyounode -g`). Run `learnyounode` and do the exercises.