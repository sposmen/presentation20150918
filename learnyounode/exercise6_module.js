var fs = require('fs'),
  path = require('path');

module.exports = function(ruta, extencion, cb){

  fs.readdir(ruta, function(err, filelist){
    if(err) {
      cb(err);
      return ;
    }
    var filtered = filelist.filter(function(file){
      return path.extname(file) == '.' + extencion;
    });
    cb(null, filtered);
  });

};

