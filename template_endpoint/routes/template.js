var express = require('express');
var router = express.Router();

/* GET template listing. */
router.get('/', function (req, res, next) {
  var db = req.app.locals.db,
    dbConn = req.app.locals.dbConn;
  db.table('data').run(dbConn).then(function (cursor) {
    return cursor.toArray()
  }).then(function (results) {
    res.json(results);
  }).error(function (err) {
    next(err);
  });
});

router.get('/:id', function (req, res, next) {
  var db = req.app.locals.db,
    dbConn = req.app.locals.dbConn;
  db.table('data').get(req.params.id).run(dbConn, function(err, data){
    res.json(data);
  });
});


/* Insert New row*/
router.post('/', function (req, res, next) {
  var db = req.app.locals.db,
    dbConn = req.app.locals.dbConn;
  console.log(req.body);
  db.table('data').insert(req.body).run(dbConn, function(err, data){
    res.json(data["generated_keys"]);
  });
});

router.put('/:id', function (req, res, next) {
  // Update specific row
  var db = req.app.locals.db,
    dbConn = req.app.locals.dbConn;
  db.table('data')
    .get(req.params.id)
    .update(req.body)
    .run(dbConn, function(err, data){
      res.json(data);
    });
});

router.delete('/:id', function (req, res, next) {
  // Delete specific row
  var db = req.app.locals.db,
    dbConn = req.app.locals.dbConn;
  db.table('data')
    .get(req.params.id)
    .delete()
    .run(dbConn, function(err, data){
      res.json(data);
    });
});


module.exports = router;
