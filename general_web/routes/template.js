var proxyMiddleware = require('http-proxy-middleware');
// create the proxy
module.exports = proxyMiddleware('/', { target: 'http://localhost:3001' });